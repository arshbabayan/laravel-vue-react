<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10),
        'description' => $faker->text(100),
        'price' => $faker->numberBetween(0,1000),
        'discount' => $faker->numberBetween(0,100),
        'quantity' => $faker->numberBetween(0,100),
        'image' => $faker->text(12),
        'status' => 'active',
    ];
});
