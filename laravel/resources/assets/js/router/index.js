import Vue from 'vue'
import Router from 'vue-router'
import Articles from '../components/Articles'
import Products from '../components/products/Products'

Vue.use(Router);

export default new Router({
    hashbang: false,
    linkActiveClass: "active",
    mode:'history',
  routes: [
      {
          path: '/articles',
          name: 'Articles',
          component: Articles,
      },
      {
          path: '/',
          // name: 'Articles',
          // redirect: '/articles',
          component: Articles,
          alias: '/articles',
          // meta: { reuse: false }
      },
      {
          path: '/products',
          name: 'Products',
          component: Products,
          // meta: { reuse: false }
      },
  ]
})

