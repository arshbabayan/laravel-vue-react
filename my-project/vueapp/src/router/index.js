import Vue from 'vue'
import Router from 'vue-router'
import Skills from '@/components/Skills'
import Test from '@/components/Test'
import Users from '@/components/Users'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base:__dirname,
  routes: [
      {
          path: '/',
          name: 'Skills',
          component: Skills,
          meta: { reuse: false }
      },
      {
          path: '/users',
          name: 'Users',
          component: Users,
          meta: { reuse: false }
      },
      {
          path: '/test',
          name: 'Test',
          component: Test,
          meta: { reuse: false }
      }
  ]
})

